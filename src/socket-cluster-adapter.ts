import { WebSocketAdapter, INestApplicationContext, Logger } from '@nestjs/common';
import { NestApplication } from '@nestjs/core';
import { MessageMappingProperties } from '@nestjs/websockets';
import { EMPTY, from, Observable, Subject } from 'rxjs';
import { flatMap, takeUntil } from 'rxjs/operators';
import { AGServer } from 'socketcluster-server';

export class SocketClusterAdapter implements WebSocketAdapter {
  private logger: Logger = new Logger("SocketClusterAdapter");
  protected readonly httpServer: any;

  constructor(private app: INestApplicationContext) {
    if (app && app instanceof NestApplication) {
      this.httpServer = app.getUnderlyingHttpServer();
    } else {
      this.httpServer = app;
    }
  }

  // This is called from websockets/socket-server-provider.ts
  // A ReplaySubject is created from the returned
  create(port: number, options: any = {}): any {
    const path = '/'
    this.logger.debug(`Creating WS Adapter on port: ${port} with options: ${JSON.stringify(options)}`);
    this.logger.debug(`SocketCluster path: ${path}`);
    return new AGServer({
      httpServer: this.httpServer,
      path,
    });
  }

  bindClientConnect(server, callback: Function) {
    (async () => {
      for await (const client of server.listener('connection')) {
        callback(client);
      }
    })();
  }

  bindMessageHandlers(
    client: any, //WebSocket,
    handlers: MessageMappingProperties[],
    transform: (data: any) => Observable<any>,
  ) {
    const { socket } = client;
    const disconnect$ = new Subject();
    const event$ = new Subject();

    (async () => {
      for await (const signal of socket.listener('disconnect')) {
        disconnect$.next(EMPTY);
      }
    })();

    (async () => {
      // Set up a loop to handle remote transmitted events.
      for await (const data of socket.receiver('customRemoteEvent')) {
        event$.next(data);
      }
    })();

    disconnect$.subscribe(() => {
      this.logger.log('Disconnected!!');
    });

    const source$ = from(event$).pipe(
      // Response is either observable or value
      flatMap(data => transform(this.bindMessageHandler(data, handlers))),
      takeUntil(disconnect$),
    );

    source$.subscribe(data => {
      socket.transmit('customRemoteEvent', data);
    });
  }

  bindMessageHandler(
    data: any,
    handlers: MessageMappingProperties[],
  ) {
    const messageHandler = handlers.find(
      handler => handler.message === 'customRemoteEvent',
    );

    // Callback is a Promise
    return messageHandler
      ? messageHandler.callback(data)
      : EMPTY;
  }

  close(server) {
    server.close();
  }
}
