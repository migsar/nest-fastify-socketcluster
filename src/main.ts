import { NestFactory } from '@nestjs/core';
import {
    FastifyAdapter,
    NestFastifyApplication,
} from '@nestjs/platform-fastify';

import { AppModule } from './app.module';
import { SocketClusterAdapter } from './socket-cluster-adapter';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter()
  );
  app.useWebSocketAdapter(new SocketClusterAdapter(app));
  await app.listen(9000, '0.0.0.0');
}
bootstrap();
