import { Logger } from '@nestjs/common';
import {
  MessageBody,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway
} from '@nestjs/websockets';
import { range } from 'rxjs';
import { map } from "rxjs/operators";

@WebSocketGateway(9000)
export class WsGateway implements OnGatewayInit {
  private logger: Logger = new Logger('WsGateway');

  afterInit() {
    this.logger.log('Initialized');
  }

  @SubscribeMessage('customRemoteEvent')
  handleMessage(@MessageBody() data: any): any {
    // What if we return an Observable?
    return range(1, 10).pipe(map(pipeData => ({ message: 'Finally', data: `${data}: ${pipeData}` })));
    // return { message: 'Finally', data };
  }
}
