# Nest.js with typescript and SocketCluster
-----

A nest server with sucketcluster support.

Currently functionality is very limited, only `'customRemoteEvent'` is being used.
The adapter for socket cluster is in `/src/socket-cluster-adapter.ts` and the gateway, used by the application is located in `/src/ws.gateway.ts`,
there is a controller but it is not connected.

## Development

### Starting the server
```bash
npm run start:dev
```

### Demo client application

```bash
cd demo
yarn serve
```
Currently socketcluster client is exposed in `App.vue` as `window.socket`. After starting the demo, which will run in http://localhost:8080:
1. Open the console. (Ctrl + Shift + i)
2. Use exposed socket as follow: `socket.transmit('customRemoteEvent', 123);`
3. An observable will be returned as a response and generate a series of events.